/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
* I certify that all code in this file is my own work.
* This code is submitted as the solution to Assignment 1
* in CSIS44542 Object-Oriented Programming, 2017, section 03
*
* Due date: 5pm, Friday, March 17, 2017.
*
* @author Akshay Reddy Vontari
*/
package sortingtest;

class SortingDriver{
    
// Method for Selection sort
public static void selectionSort(int[] array){ 
    
    int count=0;
    
        //logic for selection sort
    for (int i = 0; i < array.length - 1; i++)
    {  
            int k = i;  
            for (int j = i + 1; j < array.length; j++)
            {
                if (array[j] < array[k]){
                //Determining smallest numbers used
                    k = j;
                    count++;
                }  
            }

            int s = array[k];
            //swapping elements here
            array[k] = array[i];  
            //swapping elements here
            array[i] = s;  
    }  
        //Displaying total number of swaps
        System.out.print("Number of Swaps: "+count);
    }  

 //Method for Insertion sort 
public static void insertionSort(int[] array){
    int n = array.length;
    int count1 = 0;
    //logic for insertion sort
    for (int i = 1; i < n; i++)
    {
        int k = array[i];
        for (int j = i - 1; j > 0 && k < array[j]; j--)
        {
            //swapping elements here
            array[j+1] = array[j];
            array[j+1] = k;
            //incrementing count
            count1++;

        }
    }
    System.out.print("Number of Swaps: "+count1);
 
}
   
//Method for bubble sort
    public static void bubbleSort(int[] array) {  
        int s1;  
        int count2=0;
        //logic for bubble sort
         for(int i=0; i < array.length; i++)
        {  
        //selecting next element in array     
                 for(int j=1; j < (array.length-i); j++)
                 {  
                          if(array[j-1] > array[j])
                          {  
                              //swapping number if first number is less than second number  
                              s1= array[j-1];
                              array[j-1] = array[j];  
                              array[j] = s1;  
                              count2++;
                         }  
                          
                 }
        }

         System.out.print("Number of Swaps: "+count2);
       
       } 

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here  
        //initializing the array with 1000 numbers
        int[] orderedNumber=new int[1000];
        int[] arrayClone;
        //assigning the numbers 1 to 1000 in array 
        for(int i=0;i<orderedNumber.length;i++){
            orderedNumber[i]=i+1;
        } 
        System.out.println("Array of Ordered numbers:");
        //enhanced for loop to print numbers with space
        for(int num:orderedNumber){
        System.out.print(" "+num);
        }
        //initializing the array with 1000 numbers
        int[] reverseNumber=new int[1000];
        //assigning the numbers 1000 to 1 in array 
        for(int i=0;i<reverseNumber.length;i++){
            reverseNumber[i]=reverseNumber.length-i;
        } 
        System.out.println("\nArray of Reverse numbers:");  
        //enhanced for loop to print numbers with space
        for(int num:reverseNumber){
        System.out.print(" "+num);
        }
        //initializing the array with 1000 numbers      
        int[] randomNumber=new int[1000]; 
        //assigning the random numbers of 1000 in array 
        for(int i = 0; i < randomNumber.length; i++) {
        randomNumber[i] = (int)(Math.random()*1000);
        }
        System.out.println("\nArray of Random numbers: "); 
        //enhanced for loop to print numbers with space
        for(int num:randomNumber){
        System.out.print(" "+num);
        }
        System.out.println("\n****************************************************************");

        System.out.println("Testing for numbers in Order:\n****************************************************************");
        //To perform sorting the orderedNumbers are clone into arrayClone
        arrayClone = orderedNumber.clone();
        System.out.print("Selectionsort:     ");
        // Determining the start time before sorting performs
        long timeBeforeOrder=System.currentTimeMillis();
        selectionSort(arrayClone);
        // Determining the end time before sorting performs
        long timeAfterOrder=System.currentTimeMillis();
        // Determining the time of selection sorting
        System.out.println("\tTime Taken: "+(timeAfterOrder-timeBeforeOrder ));
        //To perform sorting the orderedNumbers are clone into arrayClone
        arrayClone = orderedNumber.clone();
        System.out.print("Insertionsort:     ");
         // Determining the start time before sorting performs
        long timeBeforeOrder1=System.currentTimeMillis();
        insertionSort(arrayClone);
        // Determining the end time after sorting performs
        long timeAfterOrder1=System.currentTimeMillis();
        // Determining the time of insertion sorting
        System.out.println("\tTime Taken: "+(timeAfterOrder1-timeBeforeOrder1 ));
        //To perform sorting the orderedNumbers are clone into arrayClone
        arrayClone = orderedNumber.clone();
        System.out.print("Bubblesort:        ");
        // Determining the start time before sorting performs
        long timeBeforeOrder2=System.currentTimeMillis();
        bubbleSort(arrayClone);
        // Determining the end time after sorting performs
        long timeAfterOrder2=System.currentTimeMillis();
        // Determining the time of bubble sorting
        System.out.println("\tTime Taken: "+(timeAfterOrder2-timeBeforeOrder2 ));
        System.out.println("****************************************************************");
        
        System.out.println("Testing for numbers in Reverse order:\n****************************************************************");
        //To perform sorting the ReverseNumbers are clone into arrayClone
        arrayClone = reverseNumber.clone();
        System.out.print("Selectionsort:     ");
        // Determining the start time before sorting performs
        long timeBeforeReverse=System.currentTimeMillis();
        selectionSort(arrayClone);
        // Determining the end time after sorting performs
        long timeAfterReverse=System.currentTimeMillis();
        //Determine the time for selection sort in reverse numbers
        System.out.println("\tTime Taken: "+(timeAfterReverse-timeBeforeReverse ));
        //To perform sorting the ReverseNumbers are clone into arrayClone
        arrayClone = reverseNumber.clone();
        System.out.print("Insertionsort:     ");
        // Determining the start time before sorting performs
        long timeBeforeReverse1=System.currentTimeMillis();
        insertionSort(arrayClone);
        // Determining the end time after sorting performs
        long timeAfterReverse1=System.currentTimeMillis();
        //Determine the time for insertion sort in reverse numbers
        System.out.println("\tTime Taken: "+(timeAfterReverse1-timeBeforeReverse1 ));
        //To perform sorting the ReverseNumbers are clone into arrayClone
        arrayClone = reverseNumber.clone();
        System.out.print("Bubblesort:        ");
        // Determining the start time before sorting performs
        long timeBeforeReverse2=System.currentTimeMillis();
        bubbleSort(arrayClone);
        // Determining the end time after sorting performs
        long timeAfterReverse2=System.currentTimeMillis();
        //Determine the time for bubble sort in reverse numbers
        System.out.println("\tTime Taken: "+(timeAfterReverse2-timeBeforeReverse2 ));
        System.out.println("****************************************************************");
        //Random Numbers
       System.out.println("Testing for Random numbers:\n****************************************************************");
       
       //To perform sorting the Random Numbers are clone into arrayClone
        arrayClone = randomNumber.clone();
        System.out.print("Selectionsort:     ");
        // Determining the start time before sorting performs
        long timeBeforeRandom=System.currentTimeMillis();
        selectionSort(arrayClone);
        // Determining the end time after sorting performs
        long timeAfterRandom=System.currentTimeMillis();
        //Determine the time for selection sort in random numbers
        System.out.println("\tTime Taken: "+(timeAfterRandom-timeBeforeRandom ));
        //To perform sorting the Random Numbers are clone into arrayClone
        arrayClone = randomNumber.clone();
        System.out.print("Insertionsort:     ");
        // Determining the start time before sorting performs
        long timeBeforeRandom1=System.currentTimeMillis();
        insertionSort(arrayClone);
        // Determining the end time after sorting performs
        long timeAfterRandom1=System.currentTimeMillis();
        //Determine the time for insertion sort in random numbers
        System.out.println("\tTime Taken: "+(timeAfterRandom1-timeBeforeRandom1 ));
        //To perform sorting the Random Numbers are clone into arrayClone
        arrayClone = randomNumber.clone();
        System.out.print("Bubblesort:        ");
        // Determining the start time before sorting performs
        long timeBeforeRandom2=System.currentTimeMillis();
        bubbleSort(arrayClone);
        // Determining the end time after sorting performs
        long timeAfterRandom2=System.currentTimeMillis();
        //Determine the time for insertion sort in random numbers
        System.out.println("\tTime Taken: "+(timeAfterRandom2-timeBeforeRandom2 ));
        System.out.println("****************************************************************");
        }
}

    
    

